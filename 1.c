#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

static int __init mmh_init(void)
{
   printk("<1>Hello,world!from the kernel space...\n");
   return 0;
}

static void __exit mmh_cleanup(void)
{
   printk("<1>Goodbye,world!leaving kernel space...\n");
}

module_init(mmh_init);
module_exit(mmh_cleanup);
MODULE_LICENSE("GPL");

